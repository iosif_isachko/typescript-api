// ~~~~~~~~ INTERFACES

interface ILocalStorageFavorites<T> {
	data(): Array<T>;
	set(data: T): void;
	remove(data: T): void;
	toggle(data: T): void;
};

// LOCAL STORAGE
function localStorageArray<T>(key: string): ILocalStorageFavorites<T> {
	let array: Array<T> = [];

	// functions
	const getData = (): string => JSON.stringify(array);
	const write = (): void => {
		const arrayString = getData();
		window.localStorage.setItem(key, arrayString);
	};

	const lsData: string | null = window.localStorage.getItem(key);
	if (lsData) {
		array = JSON.parse(lsData);
		write();
	}

	return {
		data: (): Array<T> => array,

		set: (data: T): void => {
			array.push(data);
			write();
		},

		remove: (data: T): void => {
			array = array.filter((item) => item !== data);
			write();
		},

		toggle: (data: T): void => {
			const index = array.indexOf(data);
			if (index === -1) {
				array.push(data);
			} else {
				array = array.filter((item) => item !== data);
			}
			write();
		},
	};
}

export default localStorageArray;

export {
	ILocalStorageFavorites,
};
