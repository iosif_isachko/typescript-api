// ~~~~~~~~ INTERFACES
interface ICreateElement {
	tagName: string;
	className?: string;
	attributes?: {
		[index: string]: string;
	};
};

interface IGetElement {
	root?: Document | HTMLElement;
	isAll?: boolean;
	selector: string;
};

// CONSTANTS
const XMLNS = 'http://www.w3.org/2000/svg';


// ~~~~~~~~ CREATE DOM ELEMENT
function createElement(
	{ tagName, className, attributes = {} }: ICreateElement
): HTMLElement {
	const element: HTMLElement = document.createElement(tagName);
	if (className) {
		const classNames: string[] = className.split(' ').filter(Boolean);
		element.classList.add(...classNames);
	}
	const setAttribute = (key: string): void => element.setAttribute(key, attributes[key]);
	Object.keys(attributes).forEach(setAttribute);
	return element;
}

// ~~~~~~~~ CREATE SVG ELEMENT
function createSVGElement(
	{ tagName, className, attributes = {} }: ICreateElement
): SVGElement {
	const element: SVGElement = document.createElementNS(XMLNS, tagName);
	if (className) {
		const classNames: string[] = className.split(' ').filter(Boolean);
		element.classList.add(...classNames);
	}
	const setAttribute = (key: string): void => element.setAttributeNS(null, key, attributes[key]);
	Object.keys(attributes).forEach(setAttribute);
	return element;
}


// ~~~~~~~~ GET DOM ELEMENT
function getElement(
	{ root = document, isAll = false, selector }: IGetElement
): NodeListOf<Element> | Element {
	const results = isAll
		? root.querySelectorAll(selector)
		: root.querySelector(selector);

	if(!results) {
		throw Error('Element not find.');
	}

	return results;
}


// ~~~~~~~~ EXPORT
export const DomHelper = {
	getElement,
	createElement,
	createSVGElement,
};

export {
	ICreateElement,
	IGetElement,
};
