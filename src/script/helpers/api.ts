// ~~~~~~~~ INTERFACES
interface ICallApi {
	url: string;
	options: {
		method: string;
	};
};

interface ICreateQueryString {
	apiUrl: string;
	path: Array<string>;
	queries: {
		[index: string]: string | number | boolean;
	};
};


// ~~~~~~~~ CALL API
async function callApi<T>(
	{ url, options }: ICallApi
): Promise<T> {
	return fetch(url, options)
		.then((res) => res.ok ? res.json() : Promise.reject(Error('Fail fetch.')))
		.catch((error) => { throw error; })
	;
}

// ~~~~~~~~ CREATE QUERY STRING
function creatQueryString(
	{ apiUrl, path, queries = {} }: ICreateQueryString
): string {
	let url: string = apiUrl;
	path.forEach((section) => url += `/${section}`);
	Object.keys(queries).forEach((key, index) => {
		url += index > 0 ? '&' : '?';
		url += `${key}=${queries[key]}`;
	});
	return url;
}


// ~~~~~~~~ EXPORT
export const ApiHelper = {
	callApi,
	creatQueryString,
};

export {
	ICallApi,
	ICreateQueryString,
}
