// CARD

interface ICardData {
	id: number;
	title: string;
	overview: string;
	release_date: string;
	backdrop_path: string;
}

export default ICardData;
