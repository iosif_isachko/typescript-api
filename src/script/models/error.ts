// ERROR

interface IError {
	status_message: string;
	status_code: number;
	success?: boolean;
}

export default IError;
