import IFilm from "./film";
import IFilms from "./films";
import IError from "./error";
import IMovie from './movie';
import ICardData from "./card";

export {
	IFilm,
	IFilms,
	IError,
	IMovie,
	ICardData,
};
