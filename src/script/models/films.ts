import IFilm from './film';

// FILMS

interface IFilms {
	page: number;
	results: Array<IFilm>;
	total_results: number;
	total_pages: number;
};

export default IFilms;
