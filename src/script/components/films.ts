import { ApiHelper } from '../helpers/api';
import { ICreateQueryString } from '../helpers/api';
import { IFilms, IFilm, IMovie } from '../models/_index';
import { constants } from '../constants/constants';

// ~~~~~~~~ INTERFACES

interface IGetString extends ICreateQueryString {
	queries: {
		page: number;
		[index: string]: string | number | boolean;
	};
};

interface IQueryString {
	path: Array<string>;
	queries: {
		page: number;
		[index: string]: string | number | boolean;
	};
};

interface IController {
	current: number;
	last: number;
};

interface ISearch {
	page: IController;
	data: IFilm[] | null;
	prev(): Promise<ISearch>;
	next(): Promise<ISearch>;
};

interface IByProperty {
	page?: number;
};

interface IByTitle extends IByProperty {
	title: string;
};

// ~~~~~~~~ ENUMS
enum Direction {
	Prev,
	Next,
};

// ~~~~~~~~ FILMS

interface IClassFilms {
	byTitle({ title, page }: IByTitle): Promise<ISearch>;
	byPopular({ page }: IByProperty): Promise<ISearch>;
	byTop({ page }: IByProperty): Promise<ISearch>;
	byUpcoming({ page }: IByProperty): Promise<ISearch>;
	details(id: number): Promise<IMovie | undefined>;
};

class Films implements IClassFilms {
	// Private
	private async getMovie(parameters: ICreateQueryString): Promise<IMovie | undefined> {
		const options = { method: 'GET' };
		const url: string = ApiHelper.creatQueryString(parameters);

		try {
			const film: IMovie = await ApiHelper.callApi<IMovie>({ url, options });
			return film;
		} catch (error) {
			console.error(error);
		}
	}

	private async getFilms(parameters: ICreateQueryString): Promise<IFilms | undefined> {
		const options = { method: 'GET' };
		const url: string = ApiHelper.creatQueryString(parameters);

		try {
			const films: IFilms = await ApiHelper.callApi<IFilms>({ url, options });
			return films;
		} catch (error) {
			console.error(error);
		}
	}

	private async search(parameters: IGetString): Promise<ISearch> {
		const films: IFilms | undefined = await this.getFilms(parameters);
		const page: IController = this.controller(films);

		const goTo = async (dit: Direction): Promise<ISearch> => {
			switch (dit) {
				case Direction.Prev:
					parameters.queries.page -= page.current > 1 ? 1 : 0;
					break;
				case Direction.Next:
					parameters.queries.page += page.current < page.last ? 1 : 0;
					break;
				default:
					break;
			};
			return await this.search(parameters);
		};

		return {
			page,
			data: films?.results || null,
			prev: async () => await goTo(Direction.Prev),
			next: async () => await goTo(Direction.Next),
		};
	}

	private controller(films: IFilms | undefined): IController {
		const page: IController = {
			current: films?.page || 1,
			last: films?.total_pages || 1,
		};

		return page;
	}

	private getQueryString({ path, queries }: IQueryString): IGetString {
		return {
			path,
			queries: {
				api_key: constants.API_KEY,
				...queries
			},
			apiUrl: constants.API_URL,
		};
	};

	// Public
	async byTitle({ title, page = 1 }: IByTitle): Promise<ISearch> {
		const parameters: IGetString = this.getQueryString({
			path: ['search', 'movie'],
			queries: {
				page,
				query: title.split(' ').filter(Boolean).join('+'),
			},
		});
		return this.search(parameters);
	}

	async byPopular({ page = 1 }: IByProperty = {}): Promise<ISearch> {
		const parameters: IGetString = this.getQueryString({
			path: ['movie', 'popular'],
			queries: {
				page,
			},
		});
		return this.search(parameters);
	}

	async byTop({ page = 1 }: IByProperty = {}): Promise<ISearch> {
		const parameters: IGetString = this.getQueryString({
			path: ['movie', 'top_rated'],
			queries: {
				page,
			},
		});
		return this.search(parameters);
	}

	async byUpcoming({ page = 1 }: IByProperty = {}): Promise<ISearch> {
		const parameters: IGetString = this.getQueryString({
			path: ['movie', 'upcoming'],
			queries: {
				page,
			}
		});
		return this.search(parameters);
	}

	async details(id: number): Promise<IMovie | undefined> {
		const parameters: ICreateQueryString = {
			apiUrl: constants.API_URL,
			path: ['movie', id.toString()],
			queries: {
				api_key: constants.API_KEY,
			},
		};
		return await this.getMovie(parameters);
	}
}

// ~~~~~~~~ EXPORT
export default Films;

export {
	ISearch,
};
