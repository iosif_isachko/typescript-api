import createFilmElement from '../elements/createFilmElement';
import { ILocalStorageFavorites } from '../helpers/localstorage';
import { ICardData, IFilm } from '../models/_index';
import { ICreateFilmElement } from '../elements/createFilmElement';

// ~~~~~~~~ INTERFACES
interface ICreateCard {
	data: ICardData;
	card: ICreateFilmElement;
};

// ~~~~~~~~ CONSTANTS
const IMG_URL = 'https://image.tmdb.org/t/p/original/';
const IMG_DEFAULT = './src/img/null.svg';
const DATE_DEFAULT = 'date of unknown';

// ~~~~~~~~ CARD
function getCardData(film: IFilm): ICardData {
	return {
		id: film.id,
		title: film.title,
		overview: film.overview || '',
		release_date: film.release_date || DATE_DEFAULT,
		backdrop_path: film.backdrop_path ? IMG_URL + film.backdrop_path : IMG_DEFAULT,
	};
}

function createCard(film: IFilm, ls: ILocalStorageFavorites<number>): ICreateCard {
	const cardData: ICardData = getCardData(film);
	const cardElement: ICreateFilmElement = createFilmElement(cardData);

	const onFavorite = (): void => {
		ls.toggle(cardData.id)
		cardElement.favorite();
	};

	if (ls.data().includes(cardData.id)) {
		onFavorite();
	}

	cardElement.heart.onclick = onFavorite;

	return {
		data: cardData,
		card: cardElement,
	};
}

// ~~~~~~~~ EXPORT
export default createCard;

export {
	ICreateCard
};
