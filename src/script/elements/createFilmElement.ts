import { DomHelper } from '../helpers/dom';
import { ICardData } from '../models/_index';

// ~~~~~~~~ INTERFACES
interface ICreateFilmElement {
	element: HTMLElement;
	heart: SVGElement;
	favorite(): void;
};

// ~~~~~~~~ CONSTANTS
const FILL_NONE = '#ff000078';
const FILL_ACTIVE = 'red';

// ~~~~~~~~ ELEMENTS
function createFilmElement(film: ICardData): ICreateFilmElement {
	const element: HTMLElement = DomHelper.createElement({
		tagName: 'div',
		className: 'col-lg-3 col-md-4 col-12 p-2',
	});
	const card: HTMLElement = DomHelper.createElement({
		tagName: 'div',
		className: 'card shadow-sm',
	});
	const img: HTMLElement = DomHelper.createElement({
		tagName: 'img',
		attributes: {
			src: film.backdrop_path,
		},
	});
	const favorite: SVGElement = DomHelper.createSVGElement({
		tagName: 'svg',
		className: 'bi bi-heart-fill position-absolute p-2',
		attributes: {
			stroke: 'red',
			fill: FILL_NONE,
			width: '50px',
			height: '50px',
			viewBox: '0 -2 18 22',
		},
	});
	const favoritePath: SVGElement = DomHelper.createSVGElement({
		tagName: 'path',
		attributes: {
			'fill-rule': 'evenodd',
			d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z',
		},
	});
	const cardBody: HTMLElement = DomHelper.createElement({
		tagName: 'div',
		className: 'card-body',
	});
	const description: HTMLElement = DomHelper.createElement({
		tagName: 'p',
		className: 'card-text truncate',
	});
	const date: HTMLElement = DomHelper.createElement({
		tagName: 'div',
		className: 'd-flex justify-content-between align-items-center',
	});
	const dateBody: HTMLElement = DomHelper.createElement({
		tagName: 'small',
		className: 'text-muted',
	});

	description.innerText = film.overview;
	dateBody.innerText = film.release_date;

	date.append(dateBody);
	cardBody.append(description, date);
	favorite.append(favoritePath);
	card.append(img, favorite, cardBody);
	element.append(card);

	return {
		element,
		heart: favoritePath,
		favorite: (): void => {
			const fill = favorite.getAttribute('fill');
			favorite.setAttribute('fill', fill === FILL_NONE ? FILL_ACTIVE : FILL_NONE);
		},
	};
}

// ~~~~~~~~ EXPORT
export default createFilmElement;

export {
	ICreateFilmElement,
}
