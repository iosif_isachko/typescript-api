import Films, { ISearch } from './script/components/films';
import createCard from './script/components/createCard';
import localStorageArray from './script/helpers/localstorage';
import { DomHelper } from './script/helpers/dom';
import { ICreateCard } from './script/components/createCard';


// ~~~~~~~~ DOM API
const albumElement: HTMLElement = <HTMLElement>DomHelper.getElement({ selector: '#film-container' });
const loadMoreBtn: HTMLElement = <HTMLElement>DomHelper.getElement({ selector: '#load-more' });

const popularInput: HTMLInputElement = <HTMLInputElement>DomHelper.getElement({ selector: '#popular' });
const upcomingInput: HTMLInputElement = <HTMLInputElement>DomHelper.getElement({ selector: '#upcoming' });
const topRatedInput: HTMLInputElement = <HTMLInputElement>DomHelper.getElement({ selector: '#top_rated' });

const formSearchInput: HTMLInputElement = <HTMLInputElement>DomHelper.getElement({ selector: '#search' });
const formSearchBtn: HTMLButtonElement = <HTMLButtonElement>DomHelper.getElement({ selector: '#submit' });

const randomMovieName: HTMLElement = <HTMLElement>DomHelper.getElement({ selector: '#random-movie-name' });
const randomMovieDescription: HTMLElement = <HTMLElement>DomHelper.getElement({ selector: '#random-movie-description' });

// ~~~~~~~~ CONSTANTS
const films: Films = new Films();
const favorites = localStorageArray<number>('favorites');

// ~~~~~~~~ FUNCTIONS
function random(min: number, max: number): number {
	const random = Math.random() * (max - min + 1);
	return Math.floor(random) + min;
}

function albumClear(): void {
	while(albumElement.firstChild) {
		albumElement.firstChild.remove();
	};
}

function setRandomMovie(movie: ICreateCard): void {
	randomMovieName.innerText = movie.data.title;
	randomMovieDescription.innerText = movie.data.overview;
}

function controller(movies: ISearch) {
	const cards: ICreateCard[] = [];

	const getMore = (): HTMLElement[] => {
		const moviesElements: HTMLElement[] = [];
		movies.data?.forEach((item) => {
			const film: ICreateCard = createCard(item, favorites);
			moviesElements.push(film.card.element);
			cards.push(film);
		});
		if (movies.page.current === movies.page.last) {
			loadMoreBtn.style.display = 'none';
		}
		return moviesElements;
	};

	loadMoreBtn.onclick = async () => {
		if (movies.page.current < movies.page.last) {
			movies = await movies.next();
			albumElement.append(...getMore())
		}
	};

	albumClear();
	albumElement.append(...getMore())

	if (cards.length > 0) {
		const randomCardNumber: number = random(1, cards.length);
		setRandomMovie(cards[randomCardNumber]);
	}
}

// ~~~~~~~~ ON
popularInput.onchange = async (): Promise<void> => {
	if (popularInput.checked) {
		const movies: ISearch = await films.byPopular();
		controller(movies);
	}
}

upcomingInput.onchange = async (): Promise<void> => {
	if (upcomingInput.checked) {
		const movies: ISearch = await films.byUpcoming();
		controller(movies);
	}
}

topRatedInput.onchange = async (): Promise<void> => {
	if (topRatedInput.checked) {
		const movies: ISearch = await films.byTop();
		controller(movies);
	}
}

formSearchBtn.onclick = async (): Promise<void> => {
	formSearchBtn.style.pointerEvents = 'none';
	const { value } = formSearchInput;
	if (value) {
		const movies: ISearch = await films.byTitle({ title: value.trim() });
		controller(movies);
	} else {
		popularInput.checked = true;
		const movies: ISearch = await films.byPopular();
		controller(movies);
	}
	formSearchInput.value = '';
	formSearchBtn.style.pointerEvents = 'auto';
}

// ~~~~~~~~ ~~~~~~~~ RENDER
export async function render(): Promise<void> {
	const movies: ISearch = await films.byPopular();
	controller(movies);
}
